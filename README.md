# Operation Warthog

Maksim Kotova, born June 11, 1963, Russia

Home Address:
Bol'shoy Trekhgornyy Pereulok, 4 строение 1, Moskva, Russia, 123022

Twitter: https://twitter.com/kotova_maksim

On the twitter account, we can see a screenshot of a document hosted on a Dropbox account

![](images/FHPK-KJWQAUmMw4.jpeg)

Following the link https://www.dropbox.com/sh/i8ami662bfgd849/AAC67UFpmUtgKAIMqK81MovIa we can download documents.

## Crypted files

The files with the cpt extension are crypted with the ccrypt tool. In the notes : `passw hint: daughter + restaurant (no caps one word)`.

From the twitter account, we can see these tweets :

![](images/2022-01-02_19-03_1.png)

and

![](images/2022-01-02_19-02.png)

If we isolate the sign

![](images/2022-01-02_19-03.png)

The name of the restaurant seems to be in two words, like La M|N.... On google maps, going from Kotova's place, we can search for nearby restaurants.

![](images/2022-01-02_19-29.png)

We can confirm this is the requested restaurant by going to their website and looking at their logo :

![](images/logo_g2.png)

or by going into Street View mode :

![](images/2022-01-04_15-12.png)

So we can decrypt files with this command : `ccdecrypt -K elenalamaree *.cpt* `


### ROT-47 file

The deccrypted "service" file contains a link to this text : ``%96 (9:E6 r:EJ @7 #FDD:2[ 2 8C62E s:@C2>2[ 23@G6 E96 =2?5 @7 q=F6 2?5 *6==@H[ 7C665 3J E96 #65[ 7:89E:?8 E96 vC2J[ @?6 >@C6 E:>6[ AFD9[ 4@>C256D[ @?6 >@C6 E:>6P *@F’C6 2=>@DE E96C6P pF8FDE D:I @7 7@CEJ\E9C66[ :E D92== 36 >:?6] ~? EC24<D H6 =2J[ 2E 7@FC\EH6?EJ H6 C@==[ E96 EH6?EJ\6:89E[ 5FC:?8 E96 7:CDE >@@?[ EH6?EJ\EH@[ H6 8@E J@F]` which is a ROT-47 encoded text. 

`The White City of Russia, a great Diorama, above the land of Blue and Yellow, freed by the Red, fighting the Gray, one more time, push, comrades, one more time! You’re almost there! August six of forty-three, it shall be mine. On tracks we lay, at four-twenty we roll, the twenty-eight, during the first moon, twenty-two, we got you.`

white city : Belgorod in Russia

great diorama

![](images/pic0506.jpg)

the land of blue and yellow : Ukrain

the Red : the russians

the gray : the germans

august 6, 1943 : the germans leave the city

4:20, 28, first moon, 22 : january the 28th, 2022 at 4:20

![](images/2022-01-06_18-06.png)